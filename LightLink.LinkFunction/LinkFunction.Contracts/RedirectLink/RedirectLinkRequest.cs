﻿namespace LinkFunction.Contracts.RedirectLink
{
    public class RedirectLinkRequest
    {
        public string CompressedLink { get; set; }
    }
}
