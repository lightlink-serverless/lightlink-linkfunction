﻿using Amazon.Lambda.APIGatewayEvents;

namespace Lambda.Responses
{
    public class InternalServerError : APIGatewayProxyResponse
    {
        public InternalServerError(string message)
        {
            Body = message;
            StatusCode = 500;
        }
    }
}
