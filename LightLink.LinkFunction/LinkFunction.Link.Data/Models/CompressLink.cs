﻿using Amazon.DynamoDBv2.DataModel;
using System;

namespace LinkFunction.Link.Data.Models
{
    [DynamoDBTable("Links")]
    public class CompressLink
    {
        [DynamoDBHashKey]
        public string LinkId { get; set; } = Guid.NewGuid().ToString();
        [DynamoDBRangeKey]
        public string Link { get; set; }
        public string CompressedLink { get; set; }
    }
}
