﻿using Microsoft.Extensions.DependencyInjection;

namespace Lambda.Configuration
{
    public interface IServicesConfigurableLambda
    {
        ILambdaConfiguration Configuration { get; }
        void ConfigureServices(IServiceCollection serviceCollection);
    }
}
