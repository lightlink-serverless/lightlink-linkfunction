﻿using Amazon.Lambda.APIGatewayEvents;
using System.Collections.Generic;

namespace Lambda.Responses
{
    public class ResponseBase : APIGatewayProxyResponse
    {
        public ResponseBase()
        {
            Headers = new Dictionary<string, string>() { { "Access-Control-Allow-Origin", "*" } };
            IsBase64Encoded = false;
        }
    }
}
