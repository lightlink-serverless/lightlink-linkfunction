﻿namespace LinkFunction.Contracts.RedirectLink
{
    public class RedirectLinkResponse
    {
        public string Link { get; set; }
    }
}
