﻿namespace LinkFunction.Services.Interfaces
{
    public interface ILinkCompressionService
    {
        string Compress(string link);
    }
}