﻿namespace LinkFunction.Contracts.CompressLink
{
    public class CompressLinkRequest
    {
        public string Link { get; set; }
    }
}
