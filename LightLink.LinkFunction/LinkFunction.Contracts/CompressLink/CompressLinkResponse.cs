﻿namespace LinkFunction.Contracts.CompressLink
{
    public class CompressLinkResponse
    {
        public string CompressedLink { get; set; }
    }
}
