using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using AutoMapper;
using Lambda.Configuration;
using Lambda.Responses;
using LinkFunction.Configuration;
using LinkFunction.Contracts.CompressLink;
using LinkFunction.Contracts.RedirectLink;
using LinkFunction.Link.Data.Repositories;
using LinkFunction.Link.Data.Repositories.Interfaces;
using LinkFunction.Services;
using LinkFunction.Services.Interfaces;
using LinkFunction.Services.Mappers.Profiles;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace LinkFunction
{
    public class Function : IServicesConfigurableLambda
    {
        private readonly IServiceProvider serviceProvider;
        private readonly Dictionary<Request, (string, string)> routeTable;

        public ILambdaConfiguration Configuration { get; }

        public Function()
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            serviceProvider = serviceCollection.BuildServiceProvider();
            Configuration = serviceProvider.GetService<ILambdaConfiguration>();

            routeTable = new Dictionary<Request, (string, string)>()
            {
                { Request.CompressLink, ("/links", "POST") },
                { Request.Redirect,  (@"^\/l\/[\w]{6,6}$", "GET") }
            };
        }

        public void ConfigureServices(IServiceCollection serviceCollection)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<LinkProfile>();
            });

            serviceCollection.AddSingleton<IMapper>((sp) => config.CreateMapper());

            serviceCollection.AddTransient<ILambdaConfiguration, LambdaConfiguration>();
            serviceCollection.AddTransient<ILinkRepositiry, LinkRepositiry>();
            serviceCollection.AddTransient<ILinkService, LinkService>();
            serviceCollection.AddTransient<ILinkCompressionService, LinkCompressionService>();
        }

        public async Task<APIGatewayProxyResponse> FunctionHandler(APIGatewayProxyRequest input, ILambdaContext context)
        {
            //Trace.WriteLine(context == null);
            //context.Logger.LogLine($"{input?.Path}, " +
            //    $"{input.HttpMethod}, " +
            //    (input.PathParameters.TryGetValue("link", out var value) ? $"{value}" : "null"));

            if (!routeTable[Request.CompressLink].Equals((input.Path, input.HttpMethod))
                && !(Regex.IsMatch(input.Path, routeTable[Request.Redirect].Item1) && input.HttpMethod == routeTable[Request.Redirect].Item2))
            {
                return new BadRequest();
            }

            var linkService = serviceProvider.GetService<ILinkService>();

            try
            {
                if ((input.Path, input.HttpMethod) == routeTable[Request.CompressLink])
                {
                    var request = JsonConvert.DeserializeObject<CompressLinkRequest>(input.Body);

                    var response = await linkService.AddLinkAsync(request);

                    return new Ok(JsonConvert.SerializeObject(response));
                }
                else if (Regex.IsMatch(input.Path, routeTable[Request.Redirect].Item1)
                    && input.HttpMethod == routeTable[Request.Redirect].Item2
                    && !string.IsNullOrEmpty(input.PathParameters["link"]))
                {
                    var request = new RedirectLinkRequest() { CompressedLink = input.PathParameters["link"] };

                    var response = await linkService.GetRedirectLink(request);

                    return new Redirect(response.Link);
                }
                return new BadRequest("Unsupported");
            }
            catch(AggregateException ex)
            {
                foreach(var e in ex.InnerExceptions)
                {
                    context.Logger.LogLine(e.Message);
                    context.Logger.LogLine(e.StackTrace);
                }

                return new InternalServerError(ex.Message);
            }
            catch (Exception ex)
            {
                context.Logger.LogLine(ex.Message);
                context.Logger.LogLine(ex.StackTrace);
                return new InternalServerError(ex.Message);
            }
        }
    }
}
