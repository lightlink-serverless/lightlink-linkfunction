﻿namespace Lambda.Responses
{
    public class Ok : ResponseBase
    {
        public Ok(string message)
        {
            Body = message;
            StatusCode = 200;
        }
    }
}
