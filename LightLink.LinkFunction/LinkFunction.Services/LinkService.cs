﻿using AutoMapper;
using LinkFunction.Contracts.CompressLink;
using LinkFunction.Contracts.RedirectLink;
using LinkFunction.Link.Data.Models;
using LinkFunction.Link.Data.Repositories.Interfaces;
using LinkFunction.Services.Interfaces;
using System.Threading.Tasks;

namespace LinkFunction.Services
{
    public class LinkService : ILinkService
    {
        private readonly IMapper _mapper;
        private readonly ILinkRepositiry _linkRepository;
        private readonly ILinkCompressionService _linkCompressionService;

        public LinkService(IMapper mapper, ILinkRepositiry linkRepository, ILinkCompressionService linkCompressionService)
        {
            _mapper = mapper;
            _linkRepository = linkRepository;
            _linkCompressionService = linkCompressionService;
        }

        public async Task<CompressLinkResponse> AddLinkAsync(CompressLinkRequest compressLinkRequest)
        {
            var link = _mapper.Map<CompressLink>(compressLinkRequest);
            link.CompressedLink = _linkCompressionService.Compress(link.Link);

            return _mapper
                .Map<CompressLinkResponse>(await _linkRepository
                    .AddLinkAsync(link));
        }

        public async Task<RedirectLinkResponse> GetRedirectLink(RedirectLinkRequest request)
        {
            return _mapper.Map<RedirectLinkResponse>(await _linkRepository.GetLinkByCompressedLinkAsync(request.CompressedLink));
        }
    }
}
