﻿using LinkFunction.Services.Interfaces;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace LinkFunction.Services
{
    public class LinkCompressionService : ILinkCompressionService
    {
        // 36! / 30! * 6! = 1 947 792 combinations, based on AlphaNumeric set
        private const string AlphaNumeric = "abcdefghijklmnopqrstuvwxyz1234567890";
        private const int length = 6;

        // 40! / 34! * 6! = 3 838 380 combinations, based on HMACSHA1 encoded link
        //private const string key = "41-B2-4B-B2-D9-55-BA-75-62-23-C8-9B-D0-3F-FE-36-DA-DE-17-04-94-4B-DA-D0-23-E4-10-42-8B-ED-41-3F";

        private static Random random = new Random();

        public string Compress(string link)
        {
            //var bytes = new HMACSHA1(Encoding.UTF8.GetBytes(key)).ComputeHash(Encoding.UTF8.GetBytes(link));
            //var encodedLink = BitConverter.ToString(bytes).Replace("-", "");

            return new string(Enumerable.Repeat(AlphaNumeric, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
