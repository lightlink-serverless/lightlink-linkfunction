namespace Lambda.Responses
{
    public class Redirect : ResponseBase
    {
        public Redirect(string location)
        {
            StatusCode = 308;
            Headers.Add("Location", location);
        }
    }
}
