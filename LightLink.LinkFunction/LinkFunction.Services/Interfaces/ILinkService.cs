﻿using LinkFunction.Contracts.CompressLink;
using LinkFunction.Contracts.RedirectLink;
using System.Threading.Tasks;

namespace LinkFunction.Services.Interfaces
{
    public interface ILinkService
    {
        Task<CompressLinkResponse> AddLinkAsync(CompressLinkRequest compressLinkRequest);
        Task<RedirectLinkResponse> GetRedirectLink(RedirectLinkRequest request);
    }
}