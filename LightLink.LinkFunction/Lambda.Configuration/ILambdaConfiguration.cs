﻿using Microsoft.Extensions.Configuration;

namespace Lambda.Configuration
{
    public interface ILambdaConfiguration
    {
        IConfiguration Configuration { get; }
    }
}
