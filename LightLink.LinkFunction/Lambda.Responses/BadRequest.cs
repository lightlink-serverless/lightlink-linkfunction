﻿namespace Lambda.Responses
{
    public class BadRequest : ResponseBase
    {
        public BadRequest(string message)
        {
            Body = message;
            StatusCode = 400;
        }

        public BadRequest() : this("") { }
    }
}
