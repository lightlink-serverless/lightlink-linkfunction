using AutoMapper;
using LinkFunction.Contracts.CompressLink;
using LinkFunction.Contracts.RedirectLink;
using LinkFunction.Link.Data.Models;

namespace LinkFunction.Services.Mappers.Profiles
{
    public class LinkProfile : Profile
    {
        public LinkProfile()
        {
            CreateMap<CompressLinkRequest, CompressLink>()
                .ForMember(dest => dest.Link, src => src.MapFrom(opt => opt.Link))
                .ForAllOtherMembers(src => src.Ignore());

            CreateMap<CompressLink, CompressLinkResponse>()
                .ForMember(dest => dest.CompressedLink, src => src.MapFrom(opt => opt.CompressedLink))
                .ForAllOtherMembers(src => src.Ignore());

            CreateMap<CompressLink, RedirectLinkResponse>()
                .ForMember(dest => dest.Link, src => src.MapFrom(opt => opt.Link))
                .ForAllOtherMembers(src => src.Ignore());
        }
    }
}
